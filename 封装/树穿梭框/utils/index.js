// 获取Parent节点下的所有可选中的成员
export function getAllKeys(parent, targets = []) {
  if (!parent.isDept && !parent.disabled) {
    targets.push(parent.key)
  }
  const children = parent.children
  if (children && children.length) {
    children.map((child) => getAllKeys(child, targets))
  }
  return targets
}

// 判断选中的列表是否已经存在于选中成员列表中
export function isCheckedAll(checkedKeys, selectedAll) {
  return selectedAll.some((item) => checkedKeys.includes(item))
}

// 判断部门下所有成员是否已经被选中
export function hasSelectedAll(parent, flag = true) {  
  if (!parent.children || !parent.children.length) {
    return true
  }
  // 当前层级的人员
  const persons = parent.children.filter((item) => item.isDept === false),
    // 子部门
    children = parent.children.filter((item) => item.isDept === true)
  // 当前层级所有人员都已选中
  flag = !persons.length || persons.every((item) => item.disabled === true)
  if (flag && children.length > 0) {
    const flags = children.map((item) => hasSelectedAll(item))
    flag = flags.every((item) => item === true)
  }
  return flag
}

/**
 * 判断当前部门是否为空部门 
 *  (若子部门中有成员则该部门不为空部门)
 * */
export function isEmptyDept(parent) {
  if (!parent.children) {
    return true
  }
  // 当前层级的人员
  const persons = parent.children.filter((item) => item.isDept === false),
    // 子部门
    children = parent.children.filter((item) => item.isDept === true)
  // 当前层级有人员存在
  if (persons.length) {
    return persons.every((item) => item.disabled === true)
  } else {
    const flags = children.map(isEmptyDept)
    return flags.every((item) => item === true)
  }
}