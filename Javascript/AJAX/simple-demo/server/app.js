// 引入express依赖
const express = require('express');
// 获取express实例
const app = express();

// 设置app的路由信息
// 接收到'/'的路径请求时, 返回success结果
app.get('/', (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-ALlow-Method', 'get');
  res.statusCode = 200;
  res.send('success');
});

// 服务器于本地3000端口开启
app.listen(3000, () => {
  console.log('server is running at port 3000.');
});