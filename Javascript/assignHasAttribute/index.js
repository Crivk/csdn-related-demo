// define a assignment function
function assignHasAttribute(target, assignObj) {
    // loop target Object/Array to get key
    for (const key in target) {
        // judge whether key exists
        if (key in assignObj) {
            // assign value
            target[key] = assignObj[key]
        }
    }
}

// define target Object
let bindingForm = {
    name: '',
    age: undefined,
    email: '',
    // address: ''
}

// define tested assignObj Object
let studentInfo = {
    name: 'pipi',
    age: 20,
    email: 'pipi@newland.com.cn',
    address: 'Fujian/FuZhou/MaWei/NewLand'
}

// have a test
assignHasAttribute(bindingForm, studentInfo)

//get the result: { name: 'pipi', age: 20, email: 'pipi@newland.com.cn' }
console.log(bindingForm)

