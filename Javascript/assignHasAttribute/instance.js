let entireStudentInfo = {
    name: "pipi",
    age: 20,
    address: 'Fujian/FuZhou/MaWei/NewLand',
    email: 'pipi@newland.com.cn',
}
// 可能是
// { name: 'pipi', age: 20, email: 'pipi@newland.com.cn' }
let partStudentInfo = {
    name: entireStudentInfo.name,
    age: entireStudentInfo.age,
    email: entireStudentInfo.email
}

// 也可能是:
// { name: 'pipi', age: 20, email: 'pipi@newland.com.cn' }
let {name, age, email} = entireStudentInfo;
let anotherPartStudentInfo = {
    name,
    age,
    email
}


let obj = {
    keyOne: '',
    keyTwo: ''
}
// log true
console.log('keyOne' in obj)
// log false
console.log('keyThree' in obj)

let arr = [1, , 3, 4]
// log false
console.log(1 in arr)
// log true
console.log(3 in arr)
