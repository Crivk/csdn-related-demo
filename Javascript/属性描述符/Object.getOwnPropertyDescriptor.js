const obj = {
  a: 123,
  get b() {
    console.log(this);
    return this.a;
  },
  set b (val) {
    this.a = val;
  }
}

console.log(Object.getOwnPropertyDescriptor(obj, 'b'));
console.log("获取到obj对象中的set方法:", Object.getOwnPropertyDescriptor(obj, 'b').set);
console.log("获取到obj对象中的get方法:", Object.getOwnPropertyDescriptor(obj, 'b').get);