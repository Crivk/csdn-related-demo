// const obj = {
//   b: 5
// };

// obj.a = 10;

// console.log(Object.getOwnPropertyDescriptor(obj, 'a'));
// console.log(Object.getOwnPropertyDescriptor(obj, 'b'));

// Object.defineProperty

// const obj = {
//   name: 'zhangsan'
// }

// Object.defineProperty(obj, 'privacy', {
//   value: 'can\'t see me',
//   enumerable: false
// });

// console.log(obj);
// console.log(Object.getOwnPropertyDescriptor(obj, 'privacy'));

// 替换内部值
const obj = {}

// 定制一个配置setter、getter的函数
// 如果有配置对应的别名便使用别名
// 若未设置别名则使用 _ + 参数名做为内部值
Object.defineGetterAndSetter = function(obj, props, {getter, setter, defaultValue = undefined, alias}) {
  Object.defineProperties(obj, {
    [alias ? alias : '_' + props]: {
      value: defaultValue,
      enumerable: false,
      writable: true,
      configurable: false
    },
    [props]: {
      enumerable: true,
      configurable: true,
      get: getter,
      set: setter
    }
  });
  
}

// 利用定义Getter、Setter函数给obj对象配置属性foo
Object.defineGetterAndSetter(obj, 'foo', {
  getter() {
    return this._foo;
  },
  setter(value) {
    this._foo = value;
  },
  defaultValue: 100
});

// 打印当前的foo -> 100
console.log(obj.foo);
// 利用Setter修改foo的值
obj.foo = 200;
// 再次获取foo
console.log(obj.foo);