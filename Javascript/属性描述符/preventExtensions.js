const obj = {
  name: 'zhangsan',
  age: 20,
  sex: 'male'
}

// 利用构造器提供的阻止拓展方法
// 该方法返回一个对象。引用与传入的对象一致
Object.preventExtensions(obj);

// 随之而来的查询对象是否无法拓展
// 构造器也提供了一个方法: Object.isExtensible
obj.foo = 'foo';
obj['baz'] = 'baz';

console.log(obj);
console.log(Object.isExtensible(obj));

