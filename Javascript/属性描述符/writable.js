'use strict';

const obj = {};

Object.defineProperty(obj, 'unwritten', {
  value: 'I\'m unwritten value, u can\'t change me',
  writable: false,
  enumerable: true
})

console.log(obj.unwritten);

obj.unwritten = 'I try to change the value of Obj.unwritten';

console.log(obj.unwritten);