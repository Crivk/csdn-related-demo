const obj = {};

Object.defineProperty(obj, 'param', {
  value: 'can\'t remove this param',
  writable: true,
  configurable: true,
  enumerable: true
});

Object.defineProperty(obj, 'param', {
  value: 'change param',
  configurable: false
});

console.log(Object.getOwnPropertyDescriptor(obj, 'param'));

delete obj.param;

console.log(obj);

