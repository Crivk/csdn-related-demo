// getter/setter是对象属性的描述符
// 学习过Java、C语言的同学可能对getter、setter更加熟悉
// 在JavaScript中其实也是可以使用getter、setter来获取并设置对应值的


// 我们可以直接在对象上添加上伪函数get/set
// 其实在我们使用点语法获取对象/给对象赋值时也是调用了getter/setter
// 只不过调用的是默认的[[GET]]和赋值[[PUT]]
// 因此，我们可以显示的配置get和与set
// const obj = {
//   get foo() {
//     // 当外界使用点语法获取foo时会先调用get伪函数
//     // 最后返回的时get伪函数返回的值
//     return 123;
//   },
//   set foo(value) {

//   }
// }

// console.log(obj.foo);

// // 只使用get而不是用set
// const obj = {
//   get foo () {
//     return this._foo;
//   }
// }

// // 尝试使用默认setter修改obj的foo属性
// obj.foo = 100;

// console.log(obj);

// 在词法作用域中添加内部变量
let _foo = 100;

const obj = {
  get foo () {
    return _foo;
  },
  // 注意set必须声明参数否则将会报错
  set foo (value) {
    console.log(arguments);
    // do something like update reference
    _foo = value;
  }
}

console.log(obj.foo);
obj.foo = 200;
console.log(obj.foo);