const express = require('express');
const { resolve } = require('path');

const app = express();

app.use('/public', express.static(resolve(__dirname, './public'), {
  // 开启Etag响应头
  etag: true,
  // 开启lastModified响应头
  lastModified: true,
  // 设置强缓存
  setHeaders(res){
    res.setHeader('Expires', new Date(Date.now() + 1000 * 10));
    res.setHeader('Cache-Control', 'max-age=10');
  }
}));

app.listen('3000', () => {
  console.log('server is running at port 3000');
});