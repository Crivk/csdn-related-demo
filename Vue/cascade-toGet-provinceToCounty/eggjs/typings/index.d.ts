import 'egg';

declare module 'egg' {
    interface Application {
        Sequelize: any;
        model: any;
    }
}
