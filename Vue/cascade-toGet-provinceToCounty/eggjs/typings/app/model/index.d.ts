// This file is created by egg-ts-helper@1.33.0
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportArea from '../../../app/model/area';

declare module 'egg' {
  interface IModel {
    Area: ReturnType<typeof ExportArea>;
  }
}
