// This file is created by egg-ts-helper@1.33.0
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportBase from '../../../app/controller/base';
import ExportHome from '../../../app/controller/home';
import ExportAreaIndex from '../../../app/controller/area/index';

declare module 'egg' {
  interface IController {
    base: ExportBase;
    home: ExportHome;
    area: {
      index: ExportAreaIndex;
    }
  }
}
