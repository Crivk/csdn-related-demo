import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};

  /**
   ** @description: 配置sequelize —— 连接Mysql
   */
  config.sequelize = {
    dialect: 'mysql', // 指出的数据库类型: mysql, mariadb, postgres, mssql
    database: 'demo_base', // 数据库名称
    host: 'localhost', // 数据库ip
    port: 3306, // 数据库所在端口
    username: 'root', // 数据库用户名
    password: 'root', // 数据库密码
    /**
     * 此处的define是给所有的表设置默认配置，
     * 在定义表结构时可以覆盖此处的配置
     */
    define: {
      /*
       * 取消sequelize自动将表名写为复数
       */
      freezeTableName: true,
      /*
      * 取消默认自带的created_at字段
      *  */
      createdAt: false,
      /*
      * 取消默认自带的updated_at字段
      *  */
      updatedAt: false,
    },
  };
  /**
  ** @description: 配置跨域设置
   * @param origin: 对哪些请求ip开放
   * @param allowMethods: 对哪些方法开放
  **/
  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
  };
  return config;
};
