import { Service } from 'egg';

/**
 * Test Service
 */
export default class AreaService extends Service {

  /**
     * getAreaList
     * @param pId - 父级地区的id
     */
  public async getAreaList(pId: number | string) {
    const { ctx } = this;
    return await ctx.model.Area.findAll({
      where: {
        parent_id: pId,
      },
    });
  }
}
