'use strict';

import { Application } from 'egg';
/*
*  创建area模型
*  */
export default function(app: Application) {
  const { INTEGER, STRING } = app.Sequelize;
  return app.model.define('area', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    parent_id: {
      type: INTEGER,
    },
    name: {
      type: STRING,
    },
  });
}
