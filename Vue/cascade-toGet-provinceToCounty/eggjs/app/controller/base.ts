import { Controller } from 'egg';

class BaseController extends Controller {
  public success(data?: any) {
    const { ctx } = this;
    ctx.body = {
      code: 200,
      success: true,
      data,
    };
  }

  public error(msg?: any) {
    const { ctx } = this;
    ctx.body = {
      code: 500,
      success: false,
      msg,
    };
  }
}

export default BaseController;
