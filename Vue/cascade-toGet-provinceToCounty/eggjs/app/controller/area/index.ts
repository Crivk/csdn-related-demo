import BaseController from '../base';

class AreaController extends BaseController {
  public async getAreaList() {
    const result = await this.service.area.index.getAreaList(this.ctx.query.pId);
    this.success({
      result,
    });
  }
}

export default AreaController;
