import Request from "@/lib/request";
import { AxiosInstance } from "axios";
const BASE_URL: string = 'http://127.0.0.1:7001'
const areaAxios: AxiosInstance = Request.getAxiosInstance(BASE_URL);

export default function useAreaRequest(): IAreaRequest {

  function getAreaList(pId: number): Promise<IArea[]> {
    return areaAxios.request({
      method: 'get',
      url: '/area/getAreaList',
      params: {
        pId
      }
    })
  }
  return {
    getAreaList
  }
}