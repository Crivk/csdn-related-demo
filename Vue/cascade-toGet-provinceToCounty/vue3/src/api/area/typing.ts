interface IArea {
  id: number;
  parent_id: number;
  name: string;
}

interface IAreaRequest {
  getAreaList: (pId: number) => Promise<IArea[]>
}