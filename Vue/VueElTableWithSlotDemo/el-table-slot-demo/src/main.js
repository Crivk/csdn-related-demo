import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
// import element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)

// import global component

import publicTable from "@/components/PublicTable";

Vue.component("PublicTable", publicTable)

new Vue({
    render: h => h(App),
}).$mount('#app')
