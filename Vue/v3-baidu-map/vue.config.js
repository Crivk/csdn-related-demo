const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    configureWebpack: config => {
        config.externals = {
            BMap: "BMap"
        };
        /*
        * 给scss、sass文件添加loader
        * */
        config.module.rules.push({
            test: /\.sass$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        });
    }
})
