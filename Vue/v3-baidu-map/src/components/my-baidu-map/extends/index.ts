/* 位置对象类型定义 */
export interface IPositionInfo {
    lat: number | string | undefined;
    lng: number | string | undefined;
    province: string | undefined;
    city: string | undefined;
}
