# v3-baidu-map

# 介绍
```
本项目以vue3 + ts + baidu-map搭建，以异步的方式加载地图组件、尝试着以hooks的方式
对地图进行管理、构建等。以ts对变量、函数进行约束。
```
# CSDN博客地址：[CSDN](https://blog.csdn.net/Dpl0216/article/details/126596420?spm=1001.2014.3001.5501)
# 掘金博客地址：[掘金](https://juejin.cn/post/7140963529007300638)
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

