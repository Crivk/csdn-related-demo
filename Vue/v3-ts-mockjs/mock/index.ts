import { mock } from 'mockjs';
// 引入json文件，注意要去tsconfig.json中设置
// resolveJsonModule: true，
// 否则将会飘红
import userList from './userInfo.json';
import R from "./R";

interface IMockMsg {
    body: any;
    type: string;
    url: string;
}

interface IParam {
    name?: string;
    phone?: string;
    id?: string;
    age?: string;
}

/*
 - 无法匹配到 `?` 之后的query参数
mock('/mock/getUserInfoByAge', 'get', (age: string) => {
    const filterList = userList.filter(user => user.age === age);
    R.ok().setData({
        filterList
    });
});
*/


// - 使用参数字符串解析后数组分割的方法
mock(/\/mock\/getUserInfoByAge(\?.*)?/, 'get', (msg: IMockMsg) => {
    // 抽取我们需要的数据部分
    const { url } = msg;
    console.log(msg);
    // 拆分请求参数
    const params: IParam = {};
    url.slice((url.indexOf('?') + 1))
        // split 拆分为 ['age=20','xxx=yyy']的形式
        .split('&')
        // map 遍历后返回 [{age:20},{xxx:yyy}]的形式
        .map(item => {
            const operation: Array<string> = item.split('=');
            // 若直接使用 params[operation[0]] 会报错不能使用
            params[operation[0] as keyof IParam] = operation[1];
        })
    // 根据url中解析得到的参数，从数据库数据中筛选出相关数据
    const filterList = params?.age
        ?
        userList.filter(user => user.age === params.age)
        :
        userList;
    return R.ok().setData({
        filterList
    });
});
