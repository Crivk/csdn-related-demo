type dataType = string | Object;
// 返回结果类
class R {
    // 相关的返回对象属性
    private code: number | undefined;
    private message?: string;
    private success: boolean | undefined;
    private data?: dataType;

    // 成功返回集
    static ok(): R {
        const instance = new R();
        instance.code = 200;
        instance.success = true;
        return instance
    }

    // 失败返回集
    static error(): R {
        const instance = new R();
        instance.code = 500;
        instance.success = false;
        return instance
    }

    setMessage(message: string): R {
        this.message = message;
        return this;
    }

    setData(data: dataType): R {
        this.data = data;
        return this;
    }
}


export default R;