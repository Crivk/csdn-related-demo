# v3-ts-mockjs

## 👾项目说明

> &emsp;本demo项目为Vue3+ts+mockjs中对mockjs发起带有参数的get请求之时可能会遇到的问题，如报错404

## 🤖项目安装
```
yarn install
```

### 👻项目启动
```
yarn serve
```