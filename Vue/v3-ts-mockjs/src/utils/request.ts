import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

class Request {
    private static axiosInstance: AxiosInstance | null = null;

    // 单例模式获取实例
    public static getInstance(baseURL: string) {
        this.axiosInstance = axios.create({
            baseURL: baseURL,
            timeout: 6000
        });
        this.initInterceptors();
        return this.axiosInstance;
    }

    private static initInterceptors(): void {
        // 设置post请求头
        this.axiosInstance
        &&
        (this.axiosInstance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded');
        /*
            请求拦截器
            每次请求前，如果存在token则在请求头中携带token
         */
        this.axiosInstance?.interceptors.request.use(
            (config: AxiosRequestConfig) => {
                // 配置请求头，与上方设置的默认请求头类似,注意 在最新版的
                // axios中对header的定义为: header?: AxiosRequestHeaders
                // 因此，在使用最新版axios时，header可能为undefined
                // 要先对header是否存在进行判断
                config.headers
                &&
                (config.headers['Content-Type'] = 'application/x-www-form-urlencoded');
                // token的部分省略, 可以使用Cookie、storage、vuex等方式
                return config;
            },
            (error: Error) => {
                console.log(error);
            }
        )
        // 响应拦截器
        this.axiosInstance?.interceptors.response.use((response: AxiosResponse) => {
                if (response.status === 200) {
                    return response.data;
                } else {
                    Request.errorHandler(response);
                    return response;
                }
            },
            (error: any) => {
                const { response } = error;
                if (response) {
                    Request.errorHandler(response);
                    return Promise.reject(response.data);
                } else {
                    console.log('网络连接异常,请稍后再试!');
                }
            })
    }

    // 报错响应相关设置
    private static errorHandler(response: AxiosResponse) {
        switch (response.status) {
            case 401:
                break;
            case 403:
                break;
            default:
                break;
        }
    }
}

export default Request;