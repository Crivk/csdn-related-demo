import Request from "@/utils/request";

// 生成mockAxios实例
const mockAxios = Request.getInstance('/mock');

// MockRequest相关hook的接口类型
interface IMockRequest {
    getUserInfoByAge: (age: number) => Promise<any>;
}

// 到处MockRequest hook
export default function useMockRequest(): IMockRequest {

    function getUserInfoByAge(age: number): Promise<any> {
        return mockAxios.get('/mock/getUserInfoByAge', {
            params: {
                age,
                name: '张三'
            }
        });
    }

    return {
        getUserInfoByAge
    }
}