var arr = [];
for (var i = 0; i < 10; i++) {
  arr[i] = function() {
    console.log(i);
  }
}

for (var k = 0; k < 10; k++) {
  arr[k]();
}

var i = 0;
for (; i < 10;) {
  arr[i] = function() {
    console.log(i);
  }
  i++;
}