var arr = [];
for (var i = 0; i < 10; i++) {
  ;(function(i) {
    arr[i] = function() {
      console.log(i);
    }
  }(i));
}

var arr = [];
for (var i = 0; i < 10; i++) {
    arr[i] = (function(i) {
      return function() {
        console.log(i);
      }
    }(i));
}

for (var k = 0; k < 10; k++) {
  arr[k]();
}