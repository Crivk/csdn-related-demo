const obj = {
  name: 'zhangsan'
};

// 使用方法很简单
Object.freeze(obj);

obj.name = 'lisi';

console.log(obj); // { name: 'zhangsan' }
