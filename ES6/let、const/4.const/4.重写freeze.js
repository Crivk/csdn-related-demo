Object.prototype.myFreeze = function() {
  // 根据对象调用规则, this指向调用方法的对象
  // 也就是需要冻结的对象
  for (const key in this) {
    // 判断枚举到的键名为当前对象上添加的属性
    if (Object.prototype.hasOwnProperty.call(this, key)) {
      Object.defineProperty(this, key, {
        value: this[key],
        // 是否可以修改
        writable: false,
        // 是否可以枚举
        enumerable: true,
        // 是否可以增删
        configurable: false
      });
    }
  }
}

const obj = {
  name: 'zhangsan'
}

obj.myFreeze();

obj.name = 'lisi';

console.log(obj); // { name: 'zhangsan' }