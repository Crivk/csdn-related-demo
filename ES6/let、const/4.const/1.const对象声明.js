const obj = {
  name: 'zhangsan'
};

obj.name = 'lisi';

console.log(obj); // { name: 'lisi' }

// TypeError: Assignment to constant variable.
obj = {
  name: 'lisi'
}