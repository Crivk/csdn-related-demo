export interface tableItem {
  username: String;
  age: Number | String;
  sex: String;
  detail: itemDetail;
}

export interface itemDetail {
  hobby: String;
  address: String;
  telephone: Number | String;
}
