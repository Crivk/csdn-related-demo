import { tableItem } from "./typeData";

export const tableData: tableItem[] = [
  {
    username: '陈瑾昆',
    age: 20,
    sex: '男',
    detail: {
      hobby: '英雄联盟、CS:GO' ,
      telephone: '18986931219',
      address: '吉林省白山市抚松县露水河镇'
    }
  }
];